//
//  main.m
//  Json Task
//
//  Created by IndumathiContus on 27/08/14.
//  Copyright (c) 2014 contus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
