//
//  AppDelegate.h
//  Json Task
//
//  Created by IndumathiContus on 27/08/14.
//  Copyright (c) 2014 contus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
